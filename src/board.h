#pragma once

#include "spdlog/spdlog.h"
#include <vector>

class Board
{
public:
    enum Month
    {
        Jan = 1,
        Feb = 2,
        Mar = 3,
        Apr = 4,
        May = 5,
        Jun = 6,
        Jul = 7,
        Aug = 8,
        Seq = 9,
        Oct = 10,
        Nov = 11,
        Dec = 12
    };
    enum Week
    {
        Mon = 1,
        Tues = 2,
        Wed = 3,
        Thur = 4,
        Fri = 5,
        Sat = 6,
        Sun = 7
    };
    using Day = int;
    struct Position;
    struct Path;

public:
    // Board(int width, int height) : width_(width), height_(height) 
    Board()
    {
        // Board: height x width 
        data_.assign(height_, std::vector<int>(width_, 0));

        // Handle special position
        fill(0,6);
        fill(1,6);
        fill(6,3);
        fill(6,4);
        fill(6,5);
        fill(6,6);
    }

    int width() const {return width_;}

    int height() const {return height_;}

    void setup(const Month month, const Day day, const Week week)
    {
        month_ = month;
        day_ = day;
        week_ = week;
        // place month
        int month_h = int(month/6.1);
        int month_w = int((month-month_h*6)%7)-1;
        fill(month_h, month_w);

        // place day
        int day_h = int(day/7.1);
        int day_w = int((day-day_h*7)%8)-1;
        day_h += 2;
        fill(day_h, day_w);
    }

    bool inBoard(const int i, const int j) const
    {
        if(i>=0 && i<width_ && j>=0 && j<height_) return true;
        spdlog::info("overange with postion [{}, {}]", i, j);

        return false;
    }

    bool empty(const int i, const int j) const { return inBoard(i, j) && data_[i][j] == 0;}

    bool fill(int height, int width, int val=1)
    {
        if(!inBoard(height, width)) return false;
        data_[height][width] = val;

        return true;
    }

    int value(const int i, const int j)
    {
        return data_[i][j];
    }

    void update()
    {
        for(int i=0; i<height_; ++i)
        {
            for(int j=0; j<width_; ++j)
            {
                if(data_[i][j] == 2) data_[i][j] = 0;
            }
        }
    }

    std::vector<Path> path() const {return path_;}
    
    void updatePath(Path path) {
        path_.push_back(path);
    }

    const std::vector<std::vector<int>>& data() const {return data_;}
    
    struct Position
    {
        int x;
        int y;
    };
    struct Path
    {
        Path(std::string piece, Piece::Shape shape, Position pos) : piece(piece), shape(shape), pos(pos){}
        std::string piece;
        Piece::Shape shape;
        Position pos;
    };

private:
    std::vector<std::vector<int>> data_;
    std::vector<Path> path_;
    int width_ = 7;
    int height_ = 7;
    int month_ = 1, day_ = 1, week_ = 1;
};

namespace std
{
    std::size_t vecHash(std::vector<int> const& vec) {
        std::size_t seed = vec.size();
        for(auto& i : vec) {
            seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        }
        return seed;
    }

    bool operator<(const Board& lhs, const Board& rhs)
    {
        std::size_t thisSeed = 0, otherSeed = 0;
        for(auto it : lhs.data()) thisSeed += vecHash(it);
        for(auto it : rhs.data()) otherSeed += vecHash(it);
        
        return thisSeed > otherSeed;
    }
}