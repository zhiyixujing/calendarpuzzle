#pragma once

#include <vector>
#include <queue>
#include <map>
#include <iostream>

#include "spdlog/spdlog.h"

#include "piece.h"
#include "board.h"

class Puzzle
{
public:
    Puzzle(){
        pieces_.push_back(new Rect());
        pieces_.push_back(new FlashShort());
        pieces_.push_back(new ShortL());
        pieces_.push_back(new Spill());
        pieces_.push_back(new FlashLong());
        pieces_.push_back(new TShape());
        pieces_.push_back(new LongL());
        pieces_.push_back(new RectCutAngle());
    }

    void solve(const Board::Month month, const Board::Day day)
    {
        Board board;
        board.setup(month, day, Board::Week::Mon);

        // for(auto it : pieces_)
        // {
        //     std::queue<Board>{}.swap(queue_);
        //     auto tmp = board;
        //     this->init(it, tmp);
        //     spdlog::info("Total case of piece {}, is {}", it->name(), queue_.size());
        // }

        // return ;

        this->init(pieces_[0], board);
        spdlog::info("Place piece {}, current methods {}", pieces_[0]->name(), queue_.size());

        // loop all piece
        for(int iPiece=1; iPiece<pieces_.size(); ++iPiece)
        {   
            // case number of curBoard
            int n = queue_.size();
            for(int i=0; i<n; ++i)
            {
                auto curBoard = queue_.front();

                // loop all case of each piece
                for(auto ishape: pieces_[iPiece]->shapes())
                {
                    // check if ishape is drop-able
                    for(int i=0; i<board.height()-1; ++i)
                    {
                        for(int j=0; j<board.width()-1; ++j)
                        {
                            auto rst = dropShapeAt(ishape, i, j, curBoard);
                            if(rst.first == false) continue;
                            
                            rst.second.updatePath(Board::Path{pieces_[iPiece]->name(), ishape, {i, j}});
                            queue_.push(rst.second);
                        }
                    }
                }

                // pop one 
                queue_.pop();
            }

            spdlog::info("Place piece {}, current methods {}", pieces_[iPiece]->name(), queue_.size());
        }

        dumpResult(month, day);
    }

    void init(Piece* piece, Board& board)
    {
        for(int i=0; i<board.height()-1; ++i)
        {
            for(int j=0; j<board.width()-1; ++j)
            {
                for(auto shape : piece->shapes())
                {
                    auto rst = dropShapeAt(shape, i, j, board);
                    if(rst.first)
                    {
                        rst.second.updatePath(Board::Path{piece->name(), shape, {i, j}});
                        queue_.push(rst.second);
                    }
                }
            }
        }
    }

    std::pair<bool, Board> dropShapeAt(Piece::Shape curShape, int x, int y, const Board& board)
    {
        Board newBoard = board;

        for(int i=0; i<curShape.size(); ++i) 
        {
            for(int j=0; j<curShape[i].size(); ++j)
            {
                if (curShape[i][j] == 0) {
                    continue;
                }

                auto newX = x + i;
                auto newY = y + j;

                // 越界
                if (newX >= board.height() || newY >= board.width()) {
                    return {false, newBoard};
                }

                // 重合
                if (newBoard.value(newX, newY)==1) {
                    return {false, newBoard};
                }

                newBoard.fill(newX, newY);
            }
        }

        // 剪枝, 优化的很明显
        int minA = minArea(newBoard);
        newBoard.update();
        if (minA < 5) {

            return {false, newBoard};
        }

        return {true, newBoard};
    }

private:
    int minArea(Board& board)
    {
        int minVal = 1000000;
        
        for(int i=0; i<board.height(); ++i)
        {
            for(int j=0; j<board.width(); ++j)
            {
                if(board.empty(i, j))
                {
                    int area = this->dfs(board, i, j);
                    minVal = std::min(minVal, area);
                    if(area<5) return minVal; 
                }
            }
        }

        return minVal;
    }

    int dfs(Board& board, int i, int j)
    {
        // 越界
        if (!(0 <= i && i < 7 && 0 <= j && j < 7)) {
            return 0;
        }

        // 本例中, 0 为陆地
        if (!board.empty(i, j)) {
            return 0;
        }

        // floodFill
        board.fill(i, j, 2);

        return 1 +
            + this->dfs(board, i - 1, j)
            + this->dfs(board, i + 1, j)
            + this->dfs(board, i, j - 1)
            + this->dfs(board, i, j + 1);
    }

    void dumpResult(Board::Month month, Board::Day day)
    {
        spdlog::info("Result of {}月{}日 : {}", month, day, queue_.size());
        
        int i=1;
        while (!queue_.empty())
        {
            auto curPath = queue_.front();
            
            std::cout << i++ <<  " => [";
            for(auto path : curPath.path())
            {
                std::cout << path.piece << ": " << path.pos.x << ", " << path.pos.y << "; ";
            }
            std::cout << "]\n";

            queue_.pop();
        }
    }

private:
    std::vector<Piece*> pieces_;
    std::queue<Board> queue_;
};
