
#include <iostream>
#include <queue>

#include "puzzle.h"


int main(int argc, char** argv)
{
    Puzzle puzzle;
    puzzle.solve(Board::Mar, 4);

    return 0;
}