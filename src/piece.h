#pragma once

#include <string>
#include <vector>

using std::vector;

class Piece
{
public:
    using Shape = vector<vector<int>>;
public:
    virtual ~Piece(){}

public:
    virtual std::string name() = 0;
    virtual vector<Shape> shapes() = 0;
};

class Rect : public Piece
{
public:
    std::string name() { return "Rect"; }
    vector<Shape> shapes()
    {
        vector<Shape> shape;
        shape.push_back({
            {1, 1, 1}, 
            {1, 1, 1}}
        );
        shape.push_back({
            {1, 1}, 
            {1, 1}, 
            {1, 1}}
        );
        return shape;
    }
};

class Spill : public Piece
{
public:
    std::string name() { return "Spill"; }
    vector<Shape> shapes()
    {
        vector<Shape> shape;
        shape.push_back({
            {1, 0, 1}, 
            {1, 1, 1}}
        );
        shape.push_back({
            {1, 1}, 
            {1, 0}, 
            {1, 1}}
        );
        shape.push_back({
            {1, 1, 1}, 
            {1, 0, 1}}
        );
        shape.push_back({
            {1, 1}, 
            {0, 1}, 
            {1, 1}}
        );
        return shape;
    }
};

class LongL : public Piece
{
public:
    std::string name() { return "LongL"; }
    vector<Shape> shapes()
    {
        vector<Shape> shape;
        shape.push_back({
            {1, 0}, 
            {1, 0}, 
            {1, 0}, 
            {1, 1}}
        );
        shape.push_back({
            {1, 1}, 
            {1, 0}, 
            {1, 0}, 
            {1, 0}}
        );
        shape.push_back({
            {0, 1}, 
            {0, 1}, 
            {0, 1}, 
            {1, 1}}
        );
        shape.push_back({
            {1, 1}, 
            {0, 1}, 
            {0, 1}, 
            {0, 1}}
        );
        shape.push_back({
            {1, 1, 1, 1}, 
            {0, 0, 0, 1}}
        );
        shape.push_back({
            {1, 1, 1, 1}, 
            {1, 0, 0, 0}}
        );
        shape.push_back({
            {0, 0, 0, 1}, 
            {1, 1, 1, 1}}
        );
        shape.push_back({
            {1, 0, 0, 0}, 
            {1, 1, 1, 1}}
        );

        return shape;
    }
};

class ShortL : public Piece
{
public:
    std::string name() { return "ShortL"; }
    vector<Shape> shapes()
    {
        vector<Shape> shape;
        shape.push_back({
            {1, 0, 0}, 
            {1, 0, 0}, 
            {1, 1, 1}}
        );
        shape.push_back({
            {1, 1, 1}, 
            {1, 0, 0}, 
            {1, 0, 0}}
        );
        shape.push_back({
            {1, 1, 1}, 
            {0, 0, 1}, 
            {0, 0, 1}}
        );
        shape.push_back({
            {0, 0, 1}, 
            {0, 0, 1}, 
            {1, 1, 1}}
        );

        return shape;
    }
};

class RectCutAngle : public Piece
{
public:
    std::string name() { return "RectCutAngle"; }
    vector<Shape> shapes()
    {
        vector<Shape> shape;
        shape.push_back({
            {1, 1, 1}, 
            {1, 1, 0}}
        );
        shape.push_back({
            {1, 1, 1}, 
            {0, 1, 1}}
        );
        shape.push_back({
            {0, 1, 1}, 
            {1, 1, 1}}
        );
        shape.push_back({
            {1, 1, 0}, 
            {1, 1, 1}}
        );
        shape.push_back({
            {0, 1}, 
            {1, 1}, 
            {1, 1}}
        );
        shape.push_back({
            {1, 0}, 
            {1, 1}, 
            {1, 1}}
        );
        shape.push_back({
            {1, 1}, 
            {1, 1}, 
            {1, 0}}
        );
        shape.push_back({
            {1, 1}, 
            {1, 1}, 
            {0, 1}}
        );
        return shape;
    }
};

class TShape : public Piece
{
public:
    std::string name() { return "TShape"; }
    vector<Shape> shapes()
    {
        vector<Shape> shape;
        shape.push_back({
            {1, 1, 1, 1}, 
            {0, 1, 0, 0}}
        );
        shape.push_back({
            {1, 1, 1, 1}, 
            {0, 0, 1, 0}}
        );
        shape.push_back({
            {0, 1, 0, 0}, 
            {1, 1, 1, 1}}
        );
        shape.push_back({
            {0, 0, 1, 0}, 
            {1, 1, 1, 1}}
        );
        shape.push_back({
            {1, 0},
            {1, 0}, 
            {1, 1},
            {1, 0}}
        );
        shape.push_back({
            {1, 0},
            {1, 1}, 
            {1, 0},
            {1, 0}}
        );
        shape.push_back({
            {0, 1},
            {1, 1}, 
            {0, 1},
            {0, 1}}
        );
        shape.push_back({
            {0, 1},
            {0, 1}, 
            {1, 1},
            {0, 1}}
        );
        return shape;
    }
};

class FlashLong : public Piece
{
public:
    std::string name() { return "FlashLong"; }
    vector<Shape> shapes()
    {
        vector<Shape> shape;
        shape.push_back({
            {1, 1, 0, 0}, 
            {0, 1, 1, 1}}
        );
        shape.push_back({
            {0, 0, 1, 1}, 
            {1, 1, 1, 0}}
        );
        shape.push_back({
            {0, 1, 1, 1}, 
            {1, 1, 0, 0}}
        );
        shape.push_back({
            {1, 1, 1, 0}, 
            {0, 0, 1, 1}}
        );
        shape.push_back({
            {1, 0},
            {1, 1}, 
            {0, 1},
            {0, 1}}
        );
        shape.push_back({
            {0, 1},
            {1, 1}, 
            {1, 0},
            {1, 0}}
        );
        shape.push_back({
            {0, 1},
            {0, 1}, 
            {1, 1},
            {1, 0}}
        );
        shape.push_back({
            {1, 0},
            {1, 0}, 
            {1, 1},
            {0, 1}}
        );
        return shape;
    }
};

class FlashShort : public Piece
{
public:
    std::string name() { return "FlashShort"; }
    vector<Shape> shapes()
    {
        vector<Shape> shape;
        shape.push_back({
            {1, 0, 0}, 
            {1, 1, 1}, 
            {0, 0, 1}}
        );
        shape.push_back({
            {0, 0, 1}, 
            {1, 1, 1}, 
            {1, 0, 0}}
        );
        shape.push_back({
            {0, 1, 1}, 
            {0, 1, 0}, 
            {1, 1, 0}}
        );
        shape.push_back({
            {1, 1, 0}, 
            {0, 1, 0}, 
            {0, 1, 1}}
        );
        return shape;
    }
};