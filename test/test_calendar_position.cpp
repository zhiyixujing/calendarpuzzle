
#include "gtest/gtest.h"
#include <vector>

using std::vector;

TEST(PositonTest, month)
{
    vector<vector<int>> pos = {
        {0, 0},
        {0, 1},
        {0, 2},
        {0, 3},
        {0, 4},
        {0, 5},
        {1, 0},
        {1, 1},
        {1, 2},
        {1, 3},
        {1, 4},
        {1, 5}
    };

    vector<vector<int>> rst(12, vector<int>(2));

    for(int i=1; i<13; ++i)
    {
        int month_h = int(i/6.1);
        int month_w = int((i-month_h*6)%7)-1;

        rst[i-1][0] = month_h;
        rst[i-1][1] = month_w;
    }

    ASSERT_EQ(rst, pos);
}

TEST(PositonTest, day)
{
    vector<vector<int>> pos = {
        {0, 0},
        {0, 1},
        {0, 2},
        {0, 3},
        {0, 4},
        {0, 5},
        {0, 6},

        {1, 0},
        {1, 1},
        {1, 2},
        {1, 3},
        {1, 4},
        {1, 5},
        {1, 6},

        {2, 0},
        {2, 1},
        {2, 2},
        {2, 3},
        {2, 4},
        {2, 5},
        {2, 6},

        {3, 0},
        {3, 1},
        {3, 2},
        {3, 3},
        {3, 4},
        {3, 5},
        {3, 6},

        {4, 0},
        {4, 1},
        {4, 2}
    };

    vector<vector<int>> rst(31, vector<int>(2));

    for(int i=1; i<32; ++i)
    {
        int day_h = int(i/7.1);
        int day_w = int((i-day_h*7)%8)-1;

        rst[i-1][0] = day_h;
        rst[i-1][1] = day_w;
    }

    ASSERT_EQ(rst, pos);
}