#include "gtest/gtest.h"

int argc_;
char** argv_;

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    argc_ = argc;
    argv_ = argv;

    return RUN_ALL_TESTS();
}