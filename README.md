# CalendarPuzzle

日历拼图 C++ 版本  

## 日历拼图  

该拼图游戏出自 [Dragonfjord](https://www.dragonfjord.com) 网站，原名叫 A-Puzzle-A-Day。
后经国人二次研发，推出了带星期的版本。

#### 不带星期的版本
<img src="./bin/puzzle-month-day.jpg" style="zoom:130%"/>   

#### 带星期的版本
<img src="./bin/puzzle-month-day-week.jpg" style="zoom:60%" />  

这两个游戏的玩法相同，都是将给定积木块全部拼进去以后
正好可以剩下两个或者三个空位。经验证可以拼出任意一个日期组合，且第一个日期都有多种拼法，目前我
测试的带星期版本，最少的都有140种拼法。3月12日，周六有955种拼法。如下所示的其中三种拼法。

#### 其一
<img src="./bin/method-1.png">

#### 其二
<img src="./bin/method-2.png">

#### 其三
<img src="./bin/method-3.png">


## 编译安装

该项目有命令行与 GUI 两个版本。 主分支为命令行版本，求解的是不带星期的版本，qt 分支
是带星期的版本。

#### 主分支
该版本依赖 spdlog 和 gtest，在 `etc/env.sh` 指定其目录。
1. `source etc/env.sh`
2. `mkdir build`
3. `cd build`
4. `cmake ..`
5. `make `
6. `./puzzle.exe`

#### QT 分支
使用 qt-creator 进行编译。 QT 工程文件位于 src 目录下

<img src="./bin/screenshot.png">